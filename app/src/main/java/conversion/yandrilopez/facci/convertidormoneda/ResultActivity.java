package conversion.yandrilopez.facci.convertidormoneda;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textViewResultado = (TextView) findViewById(R.id.textViewResultado);
        // 1 dolar = 0.8370
        String dato = getIntent().getExtras().getString("valor");
        Double euro = Double.parseDouble(dato);
        Double resultado = euro * 1.20 ;
        Log.e("Dato recibido", dato);
        textViewResultado.setText(resultado.toString());

    }
}
