package conversion.yandrilopez.facci.convertidormoneda;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editTextvalorDolar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextvalorDolar = (EditText)findViewById(R.id.editTextValorDolar);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cuando se presiona el boton se viene aqui en esta linea
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                //llamar al activity que muestre el resultado
                Intent intent = new Intent(MainActivity.this,ResultActivity.class);
                intent.putExtra("valor",editTextvalorDolar.getText());

                Log.e("Dato enviado",editTextvalorDolar.getText().toString());

                startActivity(intent);

            }
        });
    }

}
